Vue.config.devtools = true

Vue.component('modal', {
    template: '#modalTemplate',
    props : {
        selectedPost: "selectedPost",
        show: {
      type: Boolean,
      required: true,
      twoWay: true
    }
    }
})

var app = new Vue({
    el: '#app',
    data: {
        usersInfo: [],//store user information
        posts: [],//store post information
        userPosts: [],//store posts published by selected user
        selectedPost: {},//store data of the selected post for the modal
        showModal: false //view and hide the modal
    },
    methods: {
        //function to get the posts published by the selected user
        getPosts: function (user) {
            this.userPosts = this.posts.filter(function (post) {
                return post.userId == user.id
            });
        },
        //function to open the modal with dynamic content
        openModal:function(post){
            this.showModal = true;
            this.selectedPost = post;
            return;
        },
        //function to close the modal
        closeModal:function(){
            this.selectedPost = {}
            this.showModal = false;
            return;
        }
    },
    created() {
        //fetch user data from the api
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => {
                this.usersInfo = json
            })
        //fetch post data from the api
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => {
                this.posts = json
            })
    }

});